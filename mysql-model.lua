local mymodule = {}

-- Load libraries
modelfunctions = require("modelfunctions")
fs = require("acf.fs")
format = require("acf.format")
db = require("acf.db")
dbmodelfunctions = require("dbmodelfunctions")

-- Set variables
local conffile = "/etc/mysql/my.cnf"
local processname = "mariadb"
local packagename = "mariadb"

-- ################################################################################
-- LOCAL FUNCTIONS

local determineconnection = function()
	local port = format.parse_ini_file(fs.read_file(conffile), "client", "port")
	return db.create(db.engine.mysql, nil, nil, nil, "", tonumber(port) or 3306)
end

-- ################################################################################
-- PUBLIC FUNCTIONS

function mymodule.get_startstop(self, clientdata)
	return modelfunctions.get_startstop(processname)
end

function mymodule.startstop_service(self, startstop, action)
	return modelfunctions.startstop_service(startstop, action)
end

function mymodule.getstatus()
	return modelfunctions.getstatus(processname, packagename, "MariaDB Status")
end

function mymodule.getstatusdetails()
	local retval = cfe({ type="longtext", value="", label="MariaDB Status Details" })
	retval.value, retval.errtxt = modelfunctions.run_executable({"mysqladmin", "extended-status"})
	return retval
end

function mymodule.getfiledetails()
	return modelfunctions.getfiledetails(conffile, {conffile})
end

function mymodule.updatefiledetails(self, filedetails)
	return modelfunctions.setfiledetails(self, filedetails, {conffile})
end

function mymodule.get_logfile(self, clientdata)
	local retval = cfe({ type="group", value={}, label="Log File Configuration" })
	retval.value.facility = cfe({value="daemon", label="Syslog Facility"})
	retval.value.grep = cfe({ value="mariadb", label="Grep" })
	return retval
end

for n,f in pairs(dbmodelfunctions) do
	mymodule[n] = function(...)
		return f(determineconnection, ...)
	end
end

return mymodule
