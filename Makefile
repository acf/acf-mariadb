APP_NAME=mariadb
BASE_NAME=mysql
PACKAGE=acf-$(APP_NAME)
VERSION=0.2.0

APP_DIST=\
	mysql*	\


EXTRA_DIST=README Makefile config.mk

DISTFILES=$(APP_DIST) $(EXTRA_DIST)

TAR=tar

P=$(PACKAGE)-$(VERSION)
tarball=$(P).tar.bz2
install_dir=$(DESTDIR)/$(appdir)/$(BASE_NAME)

all:
clean:
	rm -rf $(tarball) $(P)

dist: $(tarball)

install:
	mkdir -p "$(install_dir)"
	cp -a $(APP_DIST) "$(install_dir)"
	for i in $$(ls -1 $(acflibdir)/db-*.lsp); do\
		ln -sf ../../lib/$$(basename $$i) $(install_dir)/$$(echo "$$(basename $$i)" | sed "s/db/$(BASE_NAME)/");\
	done

$(tarball):	$(DISTFILES)
	rm -rf $(P)
	mkdir -p $(P)
	cp -a $(DISTFILES) $(P)
	$(TAR) -jcf $@ $(P)
	rm -rf $(P)

# target that creates a tar package, unpacks is and install from package
dist-install: $(tarball)
	$(TAR) -jxf $(tarball)
	$(MAKE) -C $(P) install DESTDIR=$(DESTDIR)
	rm -rf $(P)

include config.mk

.PHONY: all clean dist install dist-install
